resource "aws_launch_template" "lt_asg" {
  name_prefix   = "version-1-template"
  image_id      = "${var.ec2_ami}"
  instance_type = "${var.instanceType}"
}

resource "aws_autoscaling_group" "My_ASG" {
  availability_zones = ["us-east-2a"]
  desired_capacity   = 2 
  max_size           = 3
  min_size           = 1
  launch_template {
    id      = aws_launch_template.lt_asg.id
    version = "$Latest"
  }
}
